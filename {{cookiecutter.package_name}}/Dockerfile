##### BEGIN NODE CONTAINER

FROM node:9.7-alpine

WORKDIR /app

COPY vue /app

# Install node packages
RUN yarn

# Build production artifacts
RUN npm run build

##### END NODE CONTAINER

##### BEGIN FLASK CONTAINER

FROM python:3.6-alpine3.7

WORKDIR /app

RUN pip install gunicorn && apk --no-cache add git

COPY flask/requirements.txt /app/

RUN pip install -r requirements.txt

COPY flask /app

COPY --from=0 /app/dist /app/{{cookiecutter.package_name}}/static

RUN mv /app/{{cookiecutter.package_name}}/static/static/* /app/{{cookiecutter.package_name}}/static/ && \
	python setup.py install && \
	rm -r {{cookiecutter.package_name}} setup.py requirements.txt

EXPOSE 8000

HEALTHCHECK CMD curl --fail http://127.0.0.1:8000/api/healthz || exit 1

ENTRYPOINT ["gunicorn"]

CMD ["--bind", "0.0.0.0:8000", "wsgi:app"]
##### END FLASK CONTAINER
