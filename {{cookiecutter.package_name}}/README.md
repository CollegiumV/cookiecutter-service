# {{ cookiecutter.service_name }}

## Description

{{ cookiecutter.description }}

## Usage

```bash
docker-compose up --build
```
