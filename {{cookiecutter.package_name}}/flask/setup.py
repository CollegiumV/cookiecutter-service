from setuptools import setup, find_packages

setup(
    name='{{cookiecutter.package_name}}',
    version='0.1',
    description='{{cookiecutter.description}}',
    url='{{cookiecutter.package_url}}',
    author='cvadmins',
    author_email='cvadmins@utdallas.edu',
    license='ISC',
    packages=find_packages(),
    install_requires=['Flask'],
    include_package_data=True,
    zip_safe=False)
