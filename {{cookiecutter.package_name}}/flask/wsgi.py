#!/usr/bin/env python

from {{ cookiecutter.package_name }} import AppFactory # noqa

app = AppFactory().app

# vim: ai ts=4 sts=4 et sw=4 ft=python
