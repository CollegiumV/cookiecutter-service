from collegiumv.web.factories.base import FlaskFactory

from .controllers.main import main


class AppFactory(FlaskFactory):
    CONTROLLERS = [main]


# vim: ai ts=4 sts=4 et sw=4 ft=python
