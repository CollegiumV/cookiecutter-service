from flask import Blueprint, current_app

main = Blueprint('main', __name__)


@main.route('/', methods=['GET'])
def index():
    return current_app.send_static_file('index.html')


@main.route('/healthz', methods=['GET'])
def healthz():
    return "healthy"


# vim: ai ts=4 sts=4 et sw=4 ft=python
