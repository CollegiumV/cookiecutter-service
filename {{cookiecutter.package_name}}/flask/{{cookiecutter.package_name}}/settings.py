import os


class Config:
    SECRET_KEY = os.environ.get('CV_SECRET_KEY') or 'cvcvcvcv'
    DEBUG = os.environ.get('CV_DEBUG').lower() == "true" or False


# vim: ai ts=4 sts=4 et sw=4 ft=python
